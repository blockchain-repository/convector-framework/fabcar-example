import * as yup from 'yup';
import {
  ConvectorModel,
  ReadOnly,
  Required,
  Validate
} from '@worldsibu/convector-core-model';

export class Car extends ConvectorModel<Car> {
  @ReadOnly()
  @Required()
  public readonly type = 'io.worldsibu.car';

  // make should not change
  @ReadOnly()
  @Required()
  @Validate(yup.string())
  public make: string

  //model should not change
  @ReadOnly()
  @Required()
  @Validate(yup.string())
  public model: string

  //colour can change
  @Required()
  @Validate(yup.string())
  public colour: string

  //the owner can change
  @Required()
  @Validate(yup.string())
  public owner: string
}