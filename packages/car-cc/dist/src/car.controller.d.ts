import { ConvectorController } from '@worldsibu/convector-core-controller';
import { Car } from './car.model';
export declare class CarController extends ConvectorController {
    initLedger(): Promise<void>;
    query(id: string): Promise<Car>;
    queryAll(): Promise<Car[]>;
    create(car: Car): Promise<void | Car>;
    changeOwner(id: string, owner: string): Promise<void>;
}
