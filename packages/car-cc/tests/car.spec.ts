// tslint:disable:no-unused-expression
import { join } from 'path';
import { expect } from 'chai';
import * as uuid from 'uuid/v4';
import { MockControllerAdapter } from '@worldsibu/convector-adapter-mock';
import 'mocha';

import { Car } from '../src/car.model';
import { CarControllerClient } from '../client';

describe('Car', () => {
    let modelSample: Car;
    let adapter: MockControllerAdapter;
    let carCtrl: CarControllerClient;

    before(async () => {
        const now = new Date().getTime();
        modelSample = new Car();
        modelSample.id = uuid();
        modelSample.name = 'Test';
        modelSample.created = now;
        modelSample.modified = now;
        // Mocks the blockchain execution environment
        adapter = new MockControllerAdapter();
        carCtrl = new CarControllerClient(adapter);

        await adapter.init([
            {
            version: '*',
            controller: 'CarController',
            name: join(__dirname, '..')
            }
        ]);

    });

    it('should create a default model', async () => {
    await carCtrl.create(modelSample);

    const justSavedModel = await adapter.getById<Car>(modelSample.id);

    expect(justSavedModel.id).to.exist;
    });
});